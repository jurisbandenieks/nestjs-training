import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductsModule } from './entities/products/products.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [ProductsModule, MongooseModule.forRoot('MongoDB Atlas kluster key with username, password and DB name')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
